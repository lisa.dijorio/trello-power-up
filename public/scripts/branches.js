/* global popup:false */
/* global api:false */
/* global utils:false */
/* global project:false */
/* global Promise:false */

'use strict';
var Branches = function Branches() {
};

Branches.prototype.attachBranch = function attachBranches(t, projectId, branchRef, url, name) {
  // var self = this;
  popup.askBranchName(t, projectId, branchRef, url, name)
    .then(popup.close.bind(this, t))
    .catch(function onError(error) {
      throw t.NotHandled('error occurred while attaching branch to card', error);
    });
};

Branches.prototype.checkAuth = function getAuth(authStatus) {
  return new Promise(function callback(resolve, reject) {
    if (authStatus && authStatus.authorized) {
      resolve();
    } else {
      reject();
    }
  });
};

Branches.prototype.attach = function attach(t) {
  return utils.getAuthorizationStatus(t)
    .then(this.checkAuth)
    .then(function onSuccess() {
      return project.showProjects(t, this.showProjectBranches.bind(this));
    }.bind(this), function onError() {
      return popup.openConfigure(t);
    });
};

Branches.prototype.getBranches = function getBranches(t, project) {
  return utils.getAuthToken(t)
    .then(api.getBranches.bind(api, project.id))
    .catch(function onError(error) {
      throw t.NotHandled('error occurred while getting branches from GitLab API', error);
    });
};

Branches.prototype.showProjectsCallback = function showProjectsCallback(t, project, response) {
  var self = this;
  return response.data.map(function map(branch) {
    var url = project.web_url + '/branches/' + branch.name;
    var name = branch.commit.short_id + ' . ' + project.name_with_namespace;
    return {
      text: branch.name,
      callback: function callbackWrapper(trelloInstance) {
        return self.attachBranch(trelloInstance, project.id, branch.name, url, name);
      }
    };
  });
};

Branches.prototype.showProjectBranches = function showProjectBranches(t, project) {
  return this.getBranches(t, project)
    .then(this.showProjectsCallback.bind(this, t, project))
    .then(popup.openCreateBranch.bind(this, t))
    .catch(function onError(error) {
      throw t.NotHandled('error occurred while contacting GitLab API', error);
    });
};

window.branches = new Branches();
