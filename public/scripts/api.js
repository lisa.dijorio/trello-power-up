/* eslint-disable no-console */

/* global axios:false */

function getAllProjects(auth) {
  return axios.get(auth.api + '/projects', {
    params: {
      membership: true,
      private_token: auth.token,
      with_merge_requests_enabled: true,
      simple: true,
      per_page: 100,
      order_by: 'last_activity_at'
    }
  });
}

function getAllMergeRequests(auth, projectId) {
  return axios.get(auth.api + '/projects/' + projectId + '/merge_requests', {
    params: {
      private_token: auth.token,
      view: 'simple',
      per_page: 100
    }
  });
}

function isValidCredentials(endpoint, token) {
  return axios.get(endpoint + '/version', {
    params: {
      private_token: token
    }
  })
  .then(function validCredentials() {
    return true;
  })
  .catch(function invalidCredentials() {
    return false;
  });
}

function getRecentCommits(projectId, auth) {
  return axios.get(auth.api + '/projects/' + projectId + '/repository/commits', {
    params: {
      private_token: auth.token
    }
  });
}

function getBranches(projectId, auth) {
  return axios.get(auth.api + '/projects/' + projectId + '/repository/branches', {
    params: {
      private_token: auth.token
    }
  });
}

function createBranch(auth, projectId, branchRef, branchName) {
  return axios({
    method: 'post',
    url: auth.api + '/projects/' + projectId + '/repository/branches',
    headers: {'PRIVATE-TOKEN': auth.token},
    data: {
      branch: branchName,
      ref: branchRef

    }
  }).catch(function err(error) {
    console.log(error);
  });
}

window.api = {
  getAllProjects: getAllProjects,
  getAllMergeRequests: getAllMergeRequests,
  isValidCredentials: isValidCredentials,
  getRecentCommits: getRecentCommits,
  getBranches: getBranches,
  createBranch: createBranch
};
