function parseUrl(href) {
  var anchor = document.createElement('a');
  anchor.href = href;
  return anchor;
}

function authCallback(resolve, auth) {
  var apiExists = auth && typeof auth.api !== 'undefined';
  var tokenExists = auth && typeof auth.token !== 'undefined';

  resolve({ authorized: apiExists && tokenExists });
}

function authError(t, error) {
  throw t.NotHandled('error occurred while getting authorization status from Trello', error);
}

function setAuthToken(t, api, token) {
  return t.set('member', 'private', 'auth', {
    api: api,
    token: token
  });
}

function getAuthToken(t) {
  return t.get('member', 'private', 'auth');
}

function getAuthorizationStatus(t) {
  return new TrelloPowerUp.Promise(function authPromise(resolve) {
    getAuthToken(t)
      .then(authCallback.bind(this, resolve))
      .catch(authError.bind(this, t));
  });
}

function isUrlGitLab(url) {
  return parseUrl(url).hostname === 'gitlab.com';
}

function isMergeRequest(attachment) {
  return !!attachment.url.match(/\/merge_requests\/\d+$/m);
}

function isCommit(attachment) {
  return !!attachment.url.match(/\/commit\/[a-z0-9]+$/m);
}

function isBranch(attachment) {
  return !!attachment.url.match(/\/branches\/[a-z0-9]+$/m);
}


function getAttachmentType(attachment) {
  if (isMergeRequest(attachment)) {
    return 'merge-request';
  }  else if (isBranch(attachment)) {
    return 'branches';
  }
  return 'commit';
}

function isGitLabAttachment(attachment) {
  return isUrlGitLab(attachment.url) && (isMergeRequest(attachment) || isCommit(attachment) || isBranch(attachment));
}

function sizeContainer() {
  var t = TrelloPowerUp.iframe();
  return t.sizeTo('#content');
}

window.utils = {
  parseUrl: parseUrl,
  getAuthorizationStatus: getAuthorizationStatus,
  isUrlGitLab: isUrlGitLab,
  isGitLabAttachment: isGitLabAttachment,
  sizeContainer: sizeContainer,
  setAuthToken: setAuthToken,
  getAuthToken: getAuthToken,
  getAttachmentType: getAttachmentType
};
