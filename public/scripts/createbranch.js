/* global api:false */
/* global utils:false */

var branchInput = document.querySelector('#branch-name');
var saveButton = document.querySelector('.save-btn');

function attachCreatedBranchError(t, error) {
  throw t.NotHandled('error occurred when attaching new created branch to card', error);
}

function getCreateBranchError(t, error) {
  throw t.NotHandled('error occurred when getting merge requests from GitLab API', error);
}

function attachCreatedBranch(t, url, name) {
  return t.attach({
    url: url,
    name: name
  })
// eslint-disable-next-line no-undef
  .then(popup.close.bind(this, t))
  .catch(attachCreatedBranchError.bind(this, t));
}

function createBranchCallback(t, url, branchName, name) {
  var newName = name + ' / ' + branchName;
  return attachCreatedBranch(t, url, newName);
}

// eslint-disable-next-line consistent-return
function getAuth(t, ref, projectId, url, name, authStatus) {
  if (authStatus && authStatus.authorized) {
    return utils.getAuthToken(t).
    then(function test(auth) {
      return api.createBranch(auth, projectId, ref, branchInput.value);
    })
      .then(createBranchCallback.bind(this, t, url, branchInput.value, name))
      .catch(getCreateBranchError.bind(this, t));
  }
}

function attach(t, ref, projectId, url, name) {
  return utils.getAuthorizationStatus(t)
    .then(getAuth.bind(this, t, ref, projectId, url, name));
}

function save() {
  // saveButton.classList.add('saving');
  // saveButton.innerText = 'Saving';
  // saveButton.setAttribute('disabled', 'disabled');

  var t = TrelloPowerUp.iframe();
  var ref =  t.args[1].branch_ref;
  var projectId =  t.args[1].project_id;
  var url =  t.args[1].url;
  var name = t.args[1].name;
  attach(t, ref, projectId, url, name);
}

saveButton.addEventListener('click', save);
